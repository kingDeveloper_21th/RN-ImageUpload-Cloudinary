//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Button} from 'react-native';
import ImagePicker from 'react-native-image-picker';
// const CryptoJS = require('./crypto');
const CryptoJS = require('crypto-js');

// create a component

var $this = this;
class RNImageUploader extends Component<{}> {
  constructor (props) {
    super(props);
    $this = this;
  }
  componentDidMount () {
    // RNCloudinary.config()
  }
  render() {
    return (
      <View style={styles.container}>
        <Button onPress={this.PickImageandUpload.bind(this)} title={this.props.Text} color={this.props.color} />
      </View>
    );
  }
  PickImageandUpload() {
    var options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    }
    ImagePicker.showImagePicker(options, (res) => {
      if (res.didCancel) {
      } else if (res.error) {
        $this.props.error(res.error);
      } else {
        let source = { uri : res.uri };
        $this.ImageUpload(source.uri);
      }
    });
  }

  ImageUpload (uri) {
    let timestamp = (Date.now() / 1000 | 0).toString();
    let api_key = $this.props.API_KEY;
    let api_secret = $this.props.API_SECRET;
    let cloud_name = $this.props.CLOUD_NAME;
    let hash_string = 'timestamp=' + timestamp + api_secret;
    let signature = CryptoJS.SHA1(hash_string).toString();
    let upload_url = 'https://api.cloudinary.com/v1_1/' + cloud_name + '/image/upload';

    let formdata = new FormData();
    formdata.append('file',{uri: uri, type: 'image/jpg', name: timestamp});
    formdata.append('timestamp', timestamp);
    formdata.append('api_key',api_key);
    formdata.append('signature',signature);
    const config = {
      method: 'POST',
      body: formdata
    }
    fetch(upload_url,config).then((res) => {
      console.log(res);
      var ret;
      if (res.status == 200) {
        ret = JSON.parse(res._bodyText);
        $this.props.success(ret);
      } else {
        ret = JSON.parse(res._bodyText);
        $this.props.error(ret);
      }
      // $this.props.success()
    }).catch((err) => {
      console.log(JSON.stringify(err));
      $this.props.err(err);
    });
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    fontSize: 20,
  },
});

//make this component available to the app
export default RNImageUploader;
