/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import ImageUploader from '@component/ImageUploader';
const crypto = require('./crypto');
const secret = 'open-sesame';
const has = crypto.createHmac('sha256',secret).update('abcdefg').digest('hex');
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component<{}> {
  componentWillMount () {
    alert(has);
  }
  render() {
    return (
      <View style={styles.container}>
        <ImageUploader Text={"UPloader"} API_KEY='844663476878718' API_SECRET='81B8Xs8rNlpiT4kwiI6sZilMfew' CLOUD_NAME='dioiayg1a' success={this.success.bind(this)}/>
      </View>
    );
  }

  success (res) {
    alert(JSON.stringify(res));
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  uploader: {
    margin: 20,
    width: '100%',
    height: 100
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
